package com.zadaeus.altcapsconverter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Global text selection menu activity that provides support for converting the selected text
 * to alternating-capital-letter format.
 */
public class AltCapsConverterActivity extends Activity
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		if (intent != null)
		{
			boolean readonly = intent.getBooleanExtra(Intent.EXTRA_PROCESS_TEXT_READONLY, false);
			CharSequence text = intent.getCharSequenceExtra(Intent.EXTRA_PROCESS_TEXT);

			if (!readonly && text != null)
			{
				Intent replaceIntent = new Intent();
				String convertedText = convertToAltCaps(text.toString());
				replaceIntent.putExtra(Intent.EXTRA_PROCESS_TEXT, convertedText);

				setResult(RESULT_OK, replaceIntent);
			}
		}

		finish();
	}

	/**
	 * Converts the provided string to alt-caps format.
	 *
	 * @param text The string to convert.
	 * @return The provided string in alt-caps format.
	 */
	private String convertToAltCaps(String text)
	{
		StringBuilder convertedText = new StringBuilder();

		if (text != null)
		{
			boolean isCap = true;
			for (int i = 0, length = text.length(); i < length; i++)
			{
				char character = text.charAt(i);

				if (!Character.isWhitespace(character))
				{
					character = isCap ? Character.toUpperCase(character) : Character.toLowerCase(character);
					isCap = !isCap;
				}

				convertedText.append(character);
			}
		}

		return convertedText.toString();
	}
}
